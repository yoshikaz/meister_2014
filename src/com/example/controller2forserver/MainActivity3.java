package com.example.controller2forserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.sql.Blob;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity3 extends Activity implements SensorEventListener {

	static String URL = "http://yoshikaz.tk/php/meister/";
	static String MainFile = "main.php";
	static String imageDir = "/image/";
	static String ImageFileName = "cash.jpg";
	static int PORT = 8080;

	String IPAD;

	private Thread mTaskUpPic;
	private Thread mTaskGetPic;
	private Thread mTaskConnect;

	private Socket socket;
	BufferedOutputStream writer = null;
	BufferedInputStream reader = null;

	private SensorManager mSensorManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main3);

		Point size = new Point();
		WindowManager w2 = getWindowManager();
		w2.getDefaultDisplay().getSize(size);

		Button b1 = (Button) findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		
		Button b2 = (Button) findViewById(R.id.button2);
		b2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		
		Button b3 = (Button) findViewById(R.id.button3);
		b3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});
		
		Button b4 = (Button) findViewById(R.id.button4);
		b4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static byte[] fromInt(int value) {
		int arraySize = Integer.SIZE / Byte.SIZE;
		ByteBuffer buffer = ByteBuffer.allocate(arraySize);
		return buffer.putInt(value).array();
	}

	private Runnable ConnectSocket = new Runnable() {
		@Override
		public void run() {
			try {
				socket = new Socket(IPAD, PORT);

				if (socket.isConnected() && socket != null) {
					Log.e("kyhwa","connected");
					reader = new BufferedInputStream(socket.getInputStream());
					writer = new BufferedOutputStream(socket.getOutputStream());
					Log.e("kyhwa","opend");

					writer.write(fromInt(128));
					writer.flush();
					Log.e("kyhwa","writed128");
					while (reader.read() != 128);
					Log.e("kyhwa","read 128");
					writer.write(fromInt(256));
					writer.flush();
					Log.e("kyhwa","writed256");
					mTaskGetPic = new Thread(null, GetPic, "PicGettingService");
					mTaskGetPic.start();
					Log.e("kyhwa","started");
				}

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("kyhwa",e.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("kyhwa",e.toString());
			}

		}
	};

	private Runnable GetPic = new Runnable() {
		@Override
		public void run() {
			while (true) {
				if (socket.isConnected() && socket != null) {
					try {
						writer.write(fromInt(333));// 写真要求。
						writer.flush();
						//Log.e("kyhwa","writed 333");

						BufferedOutputStream out = new BufferedOutputStream(
								new FileOutputStream(new File("cash.jpg")));
						Log.e("kyhwa","書き込み用意");
						BufferedInputStream in = new BufferedInputStream(
								socket.getInputStream());
						byte[] buf = new byte[1024];
						int len;
						while ((len = in.read(buf)) != -1) {
							out.write(buf, 0, len);
						}

						in.close();
						out.close();

						InputStream istream = getResources().getAssets().open(
								"cash.jpg");
						Bitmap bitmap = BitmapFactory.decodeStream(istream);

						ImageView d = (ImageView) findViewById(R.id.imageView1);
						d.setImageBitmap(bitmap);
						
						Log.e("kyhwa","aaa");

						// ここでこちらからの命令。

						while (reader.read() != 333);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.e("kyhwa",e.toString());
					}
				}
			}
		}
	};

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onStop() {
		super.onStop();
		mSensorManager.unregisterListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_GAME);
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_GAME);
	}
}
