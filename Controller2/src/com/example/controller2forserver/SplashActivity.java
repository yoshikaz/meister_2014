package com.example.controller2forserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends Activity {

	private UsbManager mUsbManager;
	private UsbDevice mDevice;
	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointOut;
	private UsbEndpoint mEndpointIn;
	private byte[] message = new byte[64];
	private byte[] buffer = new byte[64];

	private byte flag;
	private int result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// タイトルを非表示にします。
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// splash.xmlをViewに指定します。
		setContentView(R.layout.splash);

		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

		Button b1 = (Button) findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent cameraAct = new Intent(getApplicationContext(),
						ServerActivity.class);
				startActivity(cameraAct);
			}
		});

		Button b2 = (Button) findViewById(R.id.button2);
		b2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent Server = new Intent(getApplicationContext(),
						MainActivity2.class);
				startActivity(Server);
			}
		});

		Button b3 = (Button) findViewById(R.id.button3);
		b3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(1213);

			}
		});
		Button b4 = (Button) findViewById(R.id.button4);
		b4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(111);
			}
		});
		Button b5 = (Button) findViewById(R.id.button5);
		b5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(112);
			}
		});
		Button b6 = (Button) findViewById(R.id.button6);
		b6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(1223);
			}
		});
		Button b7 = (Button) findViewById(R.id.button7);
		b7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(21);
			}
		});
		Button b8 = (Button) findViewById(R.id.button8);
		b8.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendUSB(22);
			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();
		Intent intent = getIntent();
		String action = intent.getAction();
		UsbDevice device = (UsbDevice) intent
				.getParcelableExtra(UsbManager.EXTRA_DEVICE);
		if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
			setDevice(device);
		} else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(device)) {
			setDevice(null);
			mConnection.close();
		}
	}

	public void onPause() {
		super.onPause();
		// sendCommand(DISCONNECT);
		flag = 0;
	}

	/***** USBデバイス接続処理 *****/
	private void setDevice(UsbDevice device) {
		// デバイスインタフェース検出
		if (device.getInterfaceCount() != 1) {
			return;
		}
		UsbInterface intf = device.getInterface(0);
		// エンドポイントの検出
		if (intf.getEndpointCount() < 2) {
			return;
		}

		// OUTエンドポイントの確認
		UsbEndpoint epout = intf.getEndpoint(0);
		if (epout.getType() != UsbConstants.USB_ENDPOINT_XFER_BULK) {
			return;
		}

		// INエンドポイントの確認
		UsbEndpoint epin = intf.getEndpoint(1);
		if (epin.getType() != UsbConstants.USB_ENDPOINT_XFER_BULK) {
			return;
		}

		// デバイス定数代入
		mDevice = device;
		mEndpointOut = epout;
		mEndpointIn = epin;

		// 　接続許可確認
		if (mDevice != null) {
			UsbDeviceConnection connection = mUsbManager.openDevice(mDevice);
			if (connection != null && connection.claimInterface(intf, true)) {
				mConnection = connection;
				flag = 1;
				// Thread thread = new Thread(this);
				// thread.start();
			} else {
				mConnection = null;
			}
		}
	}

	private void sendUSB(int arg) {
		switch (arg) {
		case 1211:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1212:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1213:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x3;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1214:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x4;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1215:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x5;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1221:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1222:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1223:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x3;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1224:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x4;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1225:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x5;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 120:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x0;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 110:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x0;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 111:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 112:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 21:
			message[0] = 0x2;
			message[1] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 2, 0);
			break;
		case 22:
			message[0] = 0x2;
			message[1] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 2, 0);
			break;
		}
	}

}