package com.example.controller2forserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends Activity implements SensorEventListener {

	static int PORT = 8080;

	float[] defoltSenserVal = { (float) 0.0, (float) 0.0 };
	float[] nowSenserVal = { (float) 0.0, (float) 0.0 };

	protected final static double RAD2DEG = 180 / Math.PI;

	float[] rotationMatrix = new float[9];
	float[] gravity = new float[3];
	float[] geomagnetic = new float[3];
	float[] attitude = new float[3];

	int Control;

	final Handler handler = new Handler();

	SensorManager sensorManager;
	private byte[] message = new byte[64];

	String IPAD;

	private Thread mTaskGetPic;
	private Thread mTaskConnect;

	private Socket socket;
	BufferedReader reader = null;
	BufferedWriter writer = null;
	BufferedInputStream in = null;

	Boolean ControlFlag = false;

	int ControlSelect = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);

		Point size = new Point();
		WindowManager w2 = getWindowManager();
		w2.getDefaultDisplay().getSize(size);

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		Button b = (Button) findViewById(R.id.button1);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				IPAD = "192.168.100.107";

				mTaskConnect = new Thread(null, ConnectSocket,
						"PicGettingService");
				mTaskConnect.start();
			}
		});

		Button b4 = (Button) findViewById(R.id.button4);
		b4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ControlFlag) {
					ControlFlag = !ControlFlag;
				} else {
					ControlFlag = !ControlFlag;
					defoltSenserVal = nowSenserVal.clone();
				}
			}
		});

		Button b2 = (Button) findViewById(R.id.button2);
		b2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Control = 21;
			}
		});

		Button b3 = (Button) findViewById(R.id.button3);
		b3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Control = 22;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static byte[] fromInt(int value) {
		int arraySize = Integer.SIZE / Byte.SIZE;
		ByteBuffer buffer = ByteBuffer.allocate(arraySize);
		return buffer.putInt(value).array();
	}

	private Runnable ConnectSocket = new Runnable() {
		@Override
		public void run() {
			try {
				socket = new Socket(IPAD, PORT);
				Log.e("kyhwa", "shu");
				if (socket.isConnected() && socket != null) {
					Log.e("kyhwa", "connected");
					reader = new BufferedReader(new InputStreamReader(
							socket.getInputStream()));
					writer = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream()));
					Log.e("kyhwa", "opend");

					mTaskGetPic = new Thread(null, GetPic, "PicGettingService");
					mTaskGetPic.start();
				}

			} catch (UnknownHostException e) {
				e.printStackTrace();
				Log.e("kyhwa", e.toString());
			} catch (IOException e) {
				e.printStackTrace();
				Log.e("kyhwa", e.toString());
			}
		}
	};

	private Runnable GetPic = new Runnable() {
		@Override
		public void run() {

			while (true) {
				if (!socket.isConnected() || socket == null) {
					try {
						socket = new Socket(IPAD, PORT);
						reader = new BufferedReader(new InputStreamReader(
								socket.getInputStream()));
						writer = new BufferedWriter(new OutputStreamWriter(
								socket.getOutputStream()));
					} catch (Exception e) {

					}
				}
				if (socket.isConnected() && socket != null) {
					Log.e("kyhwa", "whileTrue!!");
					try {
						writer.write(Control + "\n");
						writer.flush();
						handler.post(new Runnable() {
							public void run() {
								TextView t4 = (TextView) findViewById(R.id.textView4);
								t4.setText("" + Control);
							}
						});
						Log.e("kyhwa", "whileTrue2!!");
						String zh = reader.readLine();
						writer.write(zh + "\n");
						Log.e("kyhwa", "zh" + zh);
						writer.flush();

						/*
						 * try { Thread.sleep(3); } catch (InterruptedException
						 * e1) { e1.printStackTrace(); }
						 */
						// String z3 = zh.replaceAll("[ ]", "");
						// Log.e("kyhwa", "string" + zh + "hhhhh");
						final int zh2 = Integer.parseInt(zh);
						final byte[] buffer = new byte[zh2];

						Log.e("kyhwa", "picStart");
						in = new BufferedInputStream(socket.getInputStream());
						/*
						 * byte[] bug = new byte[1000000]; int len = 0; int
						 * writed = 0; while ((len = in.read(bug)) !=
						 * -1&&writed>=zh2) { int i = 0; ; for (i = writed; i <
						 * len + writed; i++) { buffer[i + writed] = bug[i -
						 * writed]; } writed += i - writed; }
						 */
						final Bitmap bitmap = BitmapFactory.decodeStream(in);
						// in.read(buffer, 0, zh2);
						Log.e("kyhwa", "buffer");

						if (buffer != null) {
							Log.e("kyhwa", "handler");
							handler.post(new Runnable() {
								public void run() {

									/*
									 * Bitmap bmp = null; bmp =
									 * BitmapFactory.decodeByteArray(buffer, 0,
									 * buffer.length);
									 */
									ImageView d = (ImageView) findViewById(R.id.imageView1);
									d.setImageBitmap(bitmap);
									Log.e("kyhwa", "painted");
								}
							});
						}
						/*
						 * try { Thread.sleep(333); } catch
						 * (InterruptedException e1) { e1.printStackTrace(); }
						 */
						Log.e("kyhwa", "OK");

					} catch (IOException e) {
						Log.e("kyhwa", e.toString());
						/*
						 * try { Thread.sleep(333); } catch
						 * (InterruptedException e1) { e1.printStackTrace(); }
						 */
					}
				} else {
					/*
					 * try { Thread.sleep(333); } catch (InterruptedException
					 * e1) { e1.printStackTrace(); }
					 */
				}
				Log.e("kyhwa", "OK������");
			}
		}
	};

	public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height,
				null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
		byte[] jdata = baos.toByteArray();
		BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
		bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length,
				bitmapFatoryOptions);
		return bmp;
	}

	@Override
	protected void onResume() {
		super.onResume();

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_UI);
	}

	public void onPause() {
		super.onPause();

		sensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_MAGNETIC_FIELD:
			geomagnetic = event.values.clone();
			break;
		case Sensor.TYPE_ACCELEROMETER:
			gravity = event.values.clone();
			break;
		}

		if (geomagnetic != null && gravity != null && ControlFlag) {

			SensorManager.getRotationMatrix(rotationMatrix, null, gravity,
					geomagnetic);
			SensorManager.getOrientation(rotationMatrix, attitude);

			nowSenserVal[0] = (int) (attitude[2] * RAD2DEG);
			nowSenserVal[1] = (int) (attitude[1] * RAD2DEG);

			TextView t1 = (TextView) findViewById(R.id.textView1);
			t1.setText("" + (nowSenserVal[0]- defoltSenserVal[0]));
			TextView t2 = (TextView) findViewById(R.id.textView2);
			t2.setText("" + (nowSenserVal[1]- defoltSenserVal[1]));

			if (ControlSelect == 0) {
				if (nowSenserVal[0] - defoltSenserVal[0] > 90) {
					Control = 1215;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] > 75
						&& nowSenserVal[0] - defoltSenserVal[0] <= 90) {
					Control = 1214;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] > 55
						&& nowSenserVal[0] - defoltSenserVal[0] <= 75) {
					Control = 1213;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] > 30
						&& nowSenserVal[0] - defoltSenserVal[0] <= 55) {
					Control = 1212;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] > 10
						&& nowSenserVal[0] - defoltSenserVal[0] <= 30) {
					Control = 1211;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] < -90) {
					Control = 1225;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] < -75
						&& nowSenserVal[0] - defoltSenserVal[0] >= -90) {
					Control = 1224;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] < -55
						&& nowSenserVal[0] - defoltSenserVal[0] >= -75) {
					Control = 1223;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] < -30
						&& nowSenserVal[0] - defoltSenserVal[0] >= -55) {
					Control = 1222;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[0] - defoltSenserVal[0] < -10
						&& nowSenserVal[0] - defoltSenserVal[0] >= -30) {
					Control = 1221;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else {
					Control = 120;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				}

			} else if (ControlSelect == 1) {

				if (nowSenserVal[1] - defoltSenserVal[1] > 10) {
					Control = 111;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else if (nowSenserVal[1] - defoltSenserVal[1] < -10) {
					Control = 112;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				} else {
					Control = 110;
					TextView t3 = (TextView) findViewById(R.id.textView3);
					t3.setText("" + Control);
				}
			}else{
				ControlSelect=-1;
			}
			
			ControlSelect++;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}
}
