package com.example.controller2forserver;

import java.io.File;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

public class MainActivity extends Activity {

	static String URL = "http://yoshikaz.tk/php/meister/";
	static String MainFile = "main.php";
	static String imageDir = "/image/";
	static String ImageFileName = "cash.jpg";

	private Thread mTaskUpPic;
	private Thread mTaskGetPic;

	private Camera mCamera;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Create an instance of Camera
		//mCamera = getCameraInstance();

		Point size = new Point();
		WindowManager w2 = getWindowManager();
		w2.getDefaultDisplay().getSize(size);

		// Create our Preview view and set it as the content of our activity.
		/*mPreview = new CameraViewMain(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);*/
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private Runnable takePic = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (true) {
				try {
					Thread.sleep(30);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				ShutterCallback shutter = new ShutterCallback() {

					@Override
					public void onShutter() {
					}
				};
				PictureCallback raw = new PictureCallback() {

					@Override
					public void onPictureTaken(byte[] data, Camera camera) {
					}
				};
				PictureCallback jpeg = new PictureCallback() { // 【5】
					@Override
					public void onPictureTaken(byte[] data, Camera camera) { // 【6】
						/*
						 * FileOutputStream fos; try { fos = new
						 * FileOutputStream( "/sdcard/Hachijojima/test.jpg");
						 * Log.e("kyhwa", "saved"); fos.write(data);
						 * fos.flush(); fos.close(); } catch
						 * (FileNotFoundException e) { // TODO Auto-generated
						 * catch block e.printStackTrace(); } catch (IOException
						 * e) { // TODO Auto-generated catch block
						 * e.printStackTrace(); }
						 */

					}
				};

				
				mCamera.takePicture(shutter, raw, jpeg); // 【7】

				//break;
			}
		}
	};

	private Runnable imageUpload = new Runnable() {
		@Override
		public void run() {
			try {
				HttpClient httpClient = new DefaultHttpClient();
				// ポスト先のファイルを指定
				HttpPost httpPost = new HttpPost(URL + MainFile);
				ResponseHandler<String> responseHandler = new BasicResponseHandler();
				MultipartEntity multipartEntity = new MultipartEntity(
						HttpMultipartMode.BROWSER_COMPATIBLE);

				// ファイル名&パスを指定
				File file = new File(ImageFileName);
				FileBody fileBody = new FileBody(file);
				// KEYとファイルを指定
				multipartEntity.addPart("upfile", fileBody);

				httpPost.setEntity(multipartEntity);
				httpClient.execute(httpPost, responseHandler);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}
}
