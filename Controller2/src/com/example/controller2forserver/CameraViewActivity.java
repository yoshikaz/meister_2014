package com.example.controller2forserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CameraViewActivity extends Activity {

	Activity mActivity;
	Camera mCamera; // カメラインスタンス
	private Thread mTaskSendPic2;
	private Thread mTaskSendPic;
	int Count = 0;

	private UsbManager mUsbManager;
	private UsbDevice mDevice;
	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointOut;

	private byte[] message = new byte[64];

	ServerSocket mServer;
	private Socket mSocket;

	int Control;

	byte[] cash = new byte[1000000];
	int cashSize = 0;

	BufferedReader reader = null;
	BufferedWriter writer = null;
	BufferedOutputStream in = null;

	Boolean flag1 = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;

		FrameLayout frameLayout = new FrameLayout(this);

		// USBホストAPIインスタンス生成
		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

		SurfaceView sv = new SurfaceView(this);
		SurfaceHolder sh = sv.getHolder();
		sh.addCallback(new SurfaceHolderCbk());
		sh.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);

		frameLayout.addView(sv);
		setContentView(frameLayout);

		mTaskSendPic = new Thread(null, sendPic, "PicGettingService");
		mTaskSendPic.start();

	}

	private Runnable sendPic = new Runnable() {
		@Override
		public void run() {
			flag1 = false;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				mServer = new ServerSocket(8080);
				mSocket = mServer.accept();
				Log.e("kyhwa", "accepted");

				reader = new BufferedReader(new InputStreamReader(
						mSocket.getInputStream()));
				writer = new BufferedWriter(new OutputStreamWriter(
						mSocket.getOutputStream()));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			while (true) {
				if (!flag1) {
					try {
						int elm = reader.read();
						Log.e("kyhwa", elm + "");
						if (elm == 77) {
							mCamera.setPreviewCallback(previewCallback);
							Log.e("kyhwa", "333");
							break;
						} else if (elm == 334) {
							writer.write(334);
							writer.flush();
							Log.e("kyhwa", "334");

							int dh = reader.read();
							sendUSB(Control);
						}

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				try {
					Thread.sleep(333);
				} catch (InterruptedException e1) {
				}
			}
		}
	};

	public class SurfaceHolderCbk implements SurfaceHolder.Callback {

		/**
		 * サーフェイスが変更された時のコールバック。
		 */
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			// カメラプレビューを開始
			mCamera.startPreview();
		}

		/**
		 * サーフェイスが生成された時のコールバック
		 */
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			// カメラオープン
			mCamera = Camera.open();
			try {
				// プレビューを表示するサーフェイスホルダーを設定
				mCamera.setPreviewDisplay(holder);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * サーフェイスが破棄された時のコールバック
		 */
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			// カメラプレビューを停止
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Intent intent = getIntent();
		String action = intent.getAction();
		UsbDevice device = (UsbDevice) intent
				.getParcelableExtra(UsbManager.EXTRA_DEVICE);
		if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
			// text1.setText("アプリケーション再起動");
			setDevice(device);
		} else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(device)) {
			setDevice(null);
			mConnection.close();
			// text1.setText("デバイス　デタッチで終了");
		}
	}

	private void sendUSB(int arg) {
		switch (arg) {
		case 1211:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1212:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1213:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x3;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1214:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x4;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1215:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x1;
			message[3] = 0x5;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1221:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1222:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1223:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x3;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1224:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x4;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 1225:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x2;
			message[3] = 0x5;
			mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
			break;
		case 120:
			message[0] = 0x1;
			message[1] = 0x2;
			message[2] = 0x0;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 110:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x0;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 111:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x1;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		case 112:
			message[0] = 0x1;
			message[1] = 0x1;
			message[2] = 0x2;
			mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
			break;
		}
	}

	private void setDevice(UsbDevice device) {
		// TextView tc = (TextView) findViewById(R.id.textView1);

		// デバイスインタフェース検出
		if (device.getInterfaceCount() != 1) {
			Log.e("kyhwa", "インタフェース発見できませんでした");
			return;
		}
		UsbInterface intf = device.getInterface(0);
		// エンドポイントの検出
		if (intf.getEndpointCount() < 2) {
			Log.e("kyhwa", "エンドポイントを検出できませんでした");
			return;
		}
		Log.e("kyhwa", "エンドポイント数 =" + intf.getEndpointCount());

		// OUTエンドポイントの確認
		UsbEndpoint epout = intf.getEndpoint(0);
		if (epout.getType() != UsbConstants.USB_ENDPOINT_XFER_BULK) {
			Log.e("kyhwa", "OUTエンドポイントがバルクタイプではありません");
			return;
		}

		// デバイス定数代入
		mDevice = device;
		mEndpointOut = epout;

		// 　接続許可確認
		if (mDevice != null) {
			UsbDeviceConnection connection = mUsbManager.openDevice(mDevice);
			if (connection != null && connection.claimInterface(intf, true)) {
				mConnection = connection;
				Log.e("kyhwa", "USBデバイス接続正常完了");

				Button b3 = (Button) findViewById(R.id.button3);
				b3.setEnabled(true);
			} else {
				mConnection = null;
				Log.e("kyhwa", "USBデバイス接続失敗");
			}
		}
	}

	public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height,
				null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
		byte[] jdata = baos.toByteArray();
		BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
		bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length,
				bitmapFatoryOptions);
		return bmp;
	}

	private PreviewCallback previewCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			mCamera.setPreviewCallback(null);

			cash = data;
			cashSize = data.length;

			mTaskSendPic2 = new Thread(null, sendPic2, "PicGettingService");
			mTaskSendPic2.start();

			/*
			 * int format = mCamera.getParameters().getPreviewFormat(); YuvImage
			 * yuvimage = new YuvImage(data, format, mCamera
			 * .getParameters().getPreviewSize().width, mCamera
			 * .getParameters().getPreviewSize().height, null);
			 * 
			 * Rect rect = new Rect(0, 0,
			 * mCamera.getParameters().getPreviewSize().width, mCamera
			 * .getParameters().getPreviewSize().height);
			 * 
			 * try { File file = new
			 * File(Environment.getExternalStorageDirectory() .getPath() +
			 * "/images/"); if (!file.exists()) { file.mkdir(); } String
			 * AttachName = file.getAbsolutePath() + "/" + "cash.jpg";
			 * FileOutputStream out = new FileOutputStream(AttachName);
			 * 
			 * 
			 * 
			 * yuvimage.compressToJpeg(rect, 50, out);
			 * 
			 * out.flush(); out.close();
			 * 
			 * FileInputStream in = new FileInputStream(AttachName); byte[] buf
			 * = new byte[1024]; int len; while ((len = in.read(buf)) != -1) {
			 * writer.write(buf, 0, len); // writer.flush(); } writer.flush();
			 * in.close();
			 * 
			 * Log.e("kyhwa", "OK!!!!!!!!");
			 * 
			 * } catch (Exception e) { e.printStackTrace(); Log.e("kyhwa",
			 * e.toString()); }
			 */
		}
	};

	private Runnable sendPic2 = new Runnable() {
		@Override
		public void run() {
			flag1 = true;
			try {
				int width = mCamera.getParameters().getPreviewSize().width;
				int height = mCamera.getParameters().getPreviewSize().height;
				writer.write(width);
				writer.flush();
				int hh = reader.read();
				if (hh == width) {
					writer.write(height);
					writer.flush();
					int hw = reader.read();
					if (hw == height) {
						writer.write(cashSize + "\n");
						writer.flush();
						String zh = reader.readLine();
						if (zh == cashSize + "") {
							in = new BufferedOutputStream(
									mSocket.getOutputStream());
							in.write(cash);
							writer.flush();
						}
					}
				}

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			flag1 = false;
		}
	};

}