package com.example.controler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class CameraService extends Service {

	private final static String IP = "1.2.3.4";
	private final static int PORT = 2000;
	private Socket socket;
	private InputStream in;
	private OutputStream out;
	private boolean mThreadActive = true;
	Integer InterbalInt = 0;

	@Override
	public void onCreate() {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.mThread = new Thread(null, mTask, "dataSendingService");
		this.mThread.start();

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	private void sendData(byte[] data) {
		try {
			if (socket != null && socket.isConnected()) {
				out.write(data);
				out.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendPic() {
		byte[] data = null;
		int size;
		byte[] buffer = new byte[1024];

		try {
			if (socket != null && socket.isConnected()) {
				out.write(data);
				out.flush();
			}
			while (socket.isConnected() && socket != null) {
				size = in.read(buffer);
				if (size <= 0)
					continue;
				byte[] data2 = new byte[size];
				System.arraycopy(buffer, 0, data, 0, size);
				Log.d("kyhwa", "data length=" + data2.length);

				File cacheDir = getCacheDir();
				File file = new File(cacheDir, "liveMov" + ".jpg");
				OutputStream outn = new FileOutputStream(file);
				try {
					byte[] buf = new byte[10 * 1000 * 1000];
					int len = 0;
					while ((len = in.read(buffer)) > 0) {
						outn.write(buf, 0, len);
					}

					outn.flush();
				} finally {
					outn.close();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private Runnable mTask = new Runnable() {
		@Override
		public void run() {

			while (mThreadActive) {
				try {
					Thread.sleep(InterbalInt);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				BufferedReader reader = null;
				BufferedWriter writer = null;

				try {
					socket = new Socket(IP, PORT);
					if (socket.isConnected() && socket != null) {
						reader = new BufferedReader(new InputStreamReader(
								socket.getInputStream()));
						writer = new BufferedWriter(new OutputStreamWriter(
								socket.getOutputStream()));
					} else {
					}

					writer.write("");// 写真要求

					while (socket.isConnected() && socket != null) {
						String data = reader.readLine();
						if (data == null)
							continue;
						byte[] bytes = data.getBytes("UTF-8");

						File cacheDir = getCacheDir();
						File file = new File(cacheDir, "liveMov" + ".jpg");
						OutputStream outn = new FileOutputStream(file);
						try {

							outn.write(bytes);
							outn.flush();
						} finally {
							outn.close();
						}
					}

				} catch (Exception e) {
					// 例外キャァァァチィィィィ。
				}
				try {
					reader.close();
					writer.close();
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
	};
	private Thread mThread;
}
