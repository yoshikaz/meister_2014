package com.example.controler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;

public class CameraViewActivity extends Activity {

	Activity mActivity;
	Camera mCamera; // カメラインスタンス
	private Thread mTaskGetPic;
	int Count = 0;

	static String URL = "http://yoshikaz.tk/php/meister/";
	static String MainFile = "main.php";
	static String imageDir = "/image/";
	static String ImageFileName = "sample";

	static int Limit = 10000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;

		FrameLayout frameLayout = new FrameLayout(this);

		Button btn = new Button(this);
		btn.setText("ボタン");
		btn.setOnClickListener(new OnClickButton());

		SurfaceView sv = new SurfaceView(this);
		SurfaceHolder sh = sv.getHolder();
		sh.addCallback(new SurfaceHolderCbk());
		sh.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);

		frameLayout.addView(sv);
		frameLayout.addView(btn, layoutParams);

		setContentView(frameLayout);

		mTaskGetPic = new Thread(null, takePic, "PicGettingService");
		mTaskGetPic.start();
	}

	private Runnable takePic = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (true) {
				try {
					Thread.sleep(30);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				mCamera.setPreviewCallback(previewCallback);
			}
		}
	};

	private Runnable sendPic = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(10030);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (true) {
				try {
					Thread.sleep(3);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for (int i = 0; i < Count; i++) {

				}
			}
		}
	};

	public class SurfaceHolderCbk implements SurfaceHolder.Callback {

		/**
		 * サーフェイスが変更された時のコールバック。
		 */
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			// カメラプレビューを開始
			mCamera.startPreview();
		}

		/**
		 * サーフェイスが生成された時のコールバック
		 */
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			// カメラオープン
			mCamera = Camera.open();
			try {
				// プレビューを表示するサーフェイスホルダーを設定
				mCamera.setPreviewDisplay(holder);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * サーフェイスが破棄された時のコールバック
		 */
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			// カメラプレビューを停止
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	private PreviewCallback previewCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			// プレビューコールバックを解除
			mCamera.setPreviewCallback(null);

			// カメラのデータフォーマットを取得する
			int format = mCamera.getParameters().getPreviewFormat();
			YuvImage yuvimage = new YuvImage(data, format, mCamera
					.getParameters().getPreviewSize().width, mCamera
					.getParameters().getPreviewSize().height, null);

			Rect rect = new Rect(0, 0,
					mCamera.getParameters().getPreviewSize().width, mCamera
							.getParameters().getPreviewSize().height);

			// JPEGに変換してファイル保存
			try {
				Count++;
				File file = new File(Environment.getExternalStorageDirectory()
						.getPath() + "/Hachijojima/sample" + Count + ".jpg");
				FileOutputStream out = new FileOutputStream(file, true);
				yuvimage.compressToJpeg(rect, 50, out);
				out.close();

			} catch (Exception e) {
				e.printStackTrace();
				Log.e("kyhwa", e.toString());
			}

			// ギャラリーに反映
			/*
			 * sendBroadcast( new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(
			 * "file://" + Environment.getExternalStorageDirectory() ) ) );
			 */
		}
	};

	public class OnClickButton implements OnClickListener {

		@Override
		public void onClick(View v) {
			// プレビューコールバックを設定
			mCamera.setPreviewCallback(previewCallback);
		}

	}

}