package com.example.controler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.os.Handler;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CHUActivity extends Activity {

	private String IP = "1.2.3.4";
	private int PORT = 2000;
	private Socket socket;
	Integer InterbalInt = 200;
	int Data = 0;
	byte[] buffer = new byte[1024];
	final Handler handler = new Handler();
	OutputStream writer = null;
	InputStream reader = null;
	private Thread mThread;
	int go = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chu_activity_main);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		TextView error = (TextView) findViewById(R.id.textView1);
		error.setText("");
		TextView result = (TextView) findViewById(R.id.textView2);
		result.setText("");
		result.setGravity(Gravity.RIGHT);

		error.setTypeface(Typeface.createFromAsset(getAssets(),
				"UbuntuMonoR.ttf"));
		result.setTypeface(Typeface.createFromAsset(getAssets(),
				"UbuntuMonoR.ttf"));

		try {
			Thread.sleep(100);
		} catch (Exception e) {
		}

		mThread = new Thread(null, mTask, "dataSendingService");
		mThread.start();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static byte[] fromInt(int value) {
		int arraySize = Integer.SIZE / Byte.SIZE;
		ByteBuffer buffer = ByteBuffer.allocate(arraySize);
		return buffer.putInt(value).array();
	}

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {
			String a = "Application::State=>Starting...";
			char[] charArray = a.toCharArray();
			for (int i = 0; i < charArray.length; i++) {
				try {
					Thread.sleep(50);
				} catch (Exception e) {
				}
				sendTextViewUnRet("" + charArray[i]);
			}
			handler.post(new Runnable() {
				public void run() {
					TextView error = (TextView) findViewById(R.id.textView1);
					error.setText(error.getText() + "\n");
				}
			});

			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
			sendTextView2nonRet("[OK]");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}

			a = "Application::Socket=>Connecting...";
			charArray = a.toCharArray();
			for (int i = 0; i < charArray.length; i++) {
				try {
					Thread.sleep(50);
				} catch (Exception e) {
				}
				sendTextViewUnRet("" + charArray[i]);
			}
			handler.post(new Runnable() {
				public void run() {
					TextView error = (TextView) findViewById(R.id.textView1);
					error.setText(error.getText() + "\n");
				}
			});
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}

			sendTextView2("[TIMEOUT]");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
			a = "Application::Camera=>Starting...";
			charArray = a.toCharArray();
			for (int i = 0; i < charArray.length; i++) {
				try {
					Thread.sleep(50);
				} catch (Exception e) {
				}
				sendTextViewUnRet("" + charArray[i]);
			}
			handler.post(new Runnable() {
				public void run() {
					TextView error = (TextView) findViewById(R.id.textView1);
					error.setText(error.getText() + "\n");
				}
			});
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
			sendTextView2("[OK]");
			try {
				Thread.sleep(500);
			} catch (Exception e) {
			}
			
			Intent intent = new Intent(getApplication(), CameraActivity.class);
			startActivity(intent);
			CHUActivity.this.finish();
		}
	};

	public void sendTextView(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView1);
				error.setText(error.getText() + "\n" + a);
			}
		});
	}

	public void sendTextViewbySingle(final String a) {
		handler.post(new Runnable() {
			public void run() {
				go = 1;
				char[] charArray = a.toCharArray();
				for (int i = 0; i < charArray.length; i++) {
					sendTextViewUnRet("" + charArray[i]);
					try {
						Thread.sleep(10);
					} catch (Exception e) {
					}
				}
				handler.post(new Runnable() {
					public void run() {
						TextView error = (TextView) findViewById(R.id.textView1);
						error.setText(error.getText() + "\n");
					}
				});
				go = 0;
			}
		});
	}

	public void sendTextViewUnRet(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView1);
				error.setText(error.getText() + a);
			}
		});
	}

	public void sendTextView2(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView2);
				error.setText(error.getText() + "\n" + a);
			}
		});
	}

	public void sendTextView2nonRet(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView2);
				error.setText(error.getText() + a);
			}
		});
	}

	public void sendTextViewUnRet2(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView2);
				error.setText(error.getText() + a);
			}
		});
	}
}
