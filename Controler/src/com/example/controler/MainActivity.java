package com.example.controler;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		TextView ee = (TextView)findViewById(R.id.textView1);
		ee.setTypeface(Typeface.createFromAsset(getAssets(), 
        		"Coda-Regular.ttf"));
		
		TextView ee2 = (TextView)findViewById(R.id.textView2);
		ee2.setTypeface(Typeface.createFromAsset(getAssets(), 
        		"Coda-Regular.ttf"));
        
        ImageButton startButton=(ImageButton)findViewById(R.id.imageButton1);
        startButton.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		ImageButton startButton=(ImageButton)findViewById(R.id.imageButton1);
        		startButton.setImageResource(R.drawable.cameraushed);
        		
        		Intent cameraAct=new Intent(getApplicationContext(),CHUActivity.class);
        		startActivity(cameraAct);
        		//startService(new Intent(MainActivity.this,MainService.class));
        	}
        });
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
