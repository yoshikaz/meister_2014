package com.example.controler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;


import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CameraActivity extends Activity {

	private String IP = "1.2.3.4";
	private int PORT = 2000;
	private Socket socket;
	Integer InterbalInt = 200;
	int Data = 0;
	private Thread mThread;
	private Thread mThreadForSocket;
	private Thread mThreadRecive;
	private Thread mThreadGetPic;
	byte[] buffer = new byte[1024];
	final Handler handler = new Handler();
	OutputStream writer = null;
	InputStream reader = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.camera_activity_main);

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		/*ImageButton button2 = (ImageButton) findViewById(R.id.imageButton2);
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Data = 2;
				mThread = new Thread(null, mTask, "dataSendingService");
				mThread.start();
			}
		});*/

		/*Button NormalButton1 = (Button) findViewById(R.id.button1);
		NormalButton1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mThreadForSocket = new Thread(null, mTaskSocket,
						"dataSendingService");
				mThreadForSocket.start();
			}
		});*/

		/*Button NormalButton4 = (Button) findViewById(R.id.button4);
		NormalButton4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mThreadGetPic = new Thread(null, mTaskGetPic,
						"dataSendingService");
				mThreadGetPic.start();
			}
		});

		Button NormalButton2 = (Button) findViewById(R.id.button2);
		NormalButton2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mThreadRecive = new Thread(null, mTaskRecive,
						"dataSendingService");
				mThreadRecive.start();
			}
		});

		ImageButton button1 = (ImageButton) findViewById(R.id.imageButton1);
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Data = 1;
				mThread = new Thread(null, mTask, "dataSendingService");
				mThread.start();
			}
		});

		ImageButton button3 = (ImageButton) findViewById(R.id.imageButton3);
		button3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Data = 3;
				mThread = new Thread(null, mTask, "dataSendingService");
				mThread.start();
			}
		});

		Button NormalButton3 = (Button) findViewById(R.id.button3);
		NormalButton3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView error = (TextView) findViewById(R.id.textView1);
				error.setText("");
			}
		});*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static byte[] fromInt(int value) {
		int arraySize = Integer.SIZE / Byte.SIZE;
		ByteBuffer buffer = ByteBuffer.allocate(arraySize);
		return buffer.putInt(value).array();
	}

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {
			try {
				if (socket.isConnected() && socket != null) {
					if (writer != null) {
						byte[] elem = new byte[8];
						final byte[] elemf = elem;
						elem = fromInt(Data);

						int elm = ByteBuffer.wrap(elem).getInt();
						final int elmf = elm;
						Log.e("kyhwa", "送信データ:" + elem.toString() + ":" + elm);
						handler.post(new Runnable() {
							public void run() {
								TextView error = (TextView) findViewById(R.id.textView1);
								error.setText(error.getText() + "\n"
										+ "SendData::" + elemf.toString()
										+ "=>" + elmf);
							}
						});

						writer.write(elem);
						writer.flush();

						if (Data == 4 || true) {
							final int data = reader.read();

							Log.e("kyhwa", "" + data);
							sendTextView("" + data);
						}
					} else {
						Log.e("kyhwa", "WC");
						sendTextView("WC");
					}
				} else {
					Log.e("kyhwa", "SC");
					sendTextView("SC");
				}
			} catch (final Exception e) {
				Log.e("kynwa", e.toString());
				sendTextView(e.toString());
			}
		}
	};

	private Runnable mTaskSocket = new Runnable() {
		@Override
		public void run() {
			try {
				socket = new Socket(IP, PORT);
				sendTextView("Application::Socket=>Connecting");
				try {
					for (int ashi = 0; ashi < 5; ashi++) {
						sendTextViewUnRet(".");
						Thread.sleep(200 / 5);
					}
				} catch (Exception e) {
				}

				if (socket.isConnected() && socket != null) {
					sendTextViewUnRet("   [OK]");
					sendTextView("Socket::Writer=>Connecting...");
					writer = socket.getOutputStream();
					sendTextViewUnRet(" --- [OK]");
					sendTextView("Socket::Reader=>Connecting...");
					reader = socket.getInputStream();
					sendTextViewUnRet(" --- [OK]");

					reader.skip(7);

					Boolean flag2 = false;
					do {
						flag2 = false;

						sendTextView("Socket::Writer=>Sending [4]...");
						writer.write(fromInt(4));
						writer.flush();
						sendTextViewUnRet(" --- [OK]");

						Boolean flag1 = false;
						int readDataInt = 0;
						do {
							flag1 = false;
							try {
								sendTextView("Socket::Reader=>Waiting...");
								readDataInt = reader.read();
								sendTextView("Socket::Reader=>Reading...");
							} catch (Exception e) {
								try {
									Thread.sleep(200);
								} catch (Exception e1) {
								}
								sendTextView("Socket::Reader=>ERROR ./Read 4");
								flag1 = true;
							}
						} while (flag1);

						if (readDataInt != 4) {
							sendTextView("Socket::Reader=>WRONG ./Read 4");
						} else {
							sendTextViewUnRet(" --- [OK]");
						}

						while (readDataInt != 4) {
						}

						sendTextView("Socket::Writer=>Sending [5]...");
						writer.write(fromInt(5));
						writer.flush();
						sendTextViewUnRet(" --- [OK]");

					} while (flag2);
				} else {
					sendTextView("Socket::State=>Closed");
				}
			} catch (final Exception e) {
				sendTextView("Application::State=>ERROR :" + e.toString());
			}
		}
	};

	private Runnable mTaskRecive = new Runnable() {
		@Override
		public void run() {
			try {
				if (socket.isConnected() && socket != null) {
					final int data = reader.read();

					Log.e("kyhwa", "" + data);
					sendTextView("" + data);
				} else {
					Log.e("kyhwa", "SC");
					sendTextView("SC");
				}
			} catch (final Exception e) {
				Log.e("kynwa", e.toString());
				sendTextView(e.toString());
			}
		}
	};

	private Runnable mTaskGetPic = new Runnable() {
		@Override
		public void run() {
			try {
				if (socket.isConnected() && socket != null) {
					writer.write(fromInt(6));
					writer.flush();

					int data = reader.read();
					Log.e("kyhwa", "画像やってきた:" + data);
					if (data == 6) {
						sendTextView("画像を生成します。");
						File cacheDir = getCacheDir();
						File file = new File(cacheDir, "liveMov" + ".jpg");
						OutputStream outn = new FileOutputStream(file);
						sendTextView("書き込み開始。");
						for (int i = 0; i < data; i++) {
							int buffer = reader.read();
							try {
								byte[] buf = fromInt(buffer);
								for (int j = 0; j < buf.length; j++) {
									outn.write(buf[j]);
								}
								outn.flush();
							} finally {
							}
						}
						sendTextView("画像を生成しました。");
						outn.close();

						handler.post(new Runnable() {
							public void run() {
								sendTextView("画像を読み込みます。");
								ImageView a = (ImageView) findViewById(R.id.imageView1);

								File file = new File(getCacheDir(), "liveMov"
										+ ".jpg");
								InputStream is = null;
								Bitmap bm = null;
								try {
									is = new FileInputStream(file);
									if (is != null) {
										bm = BitmapFactory.decodeStream(is);
										a.setImageBitmap(bm);
									}
									sendTextView("画像を読み込みました。");
									is.close();

									if (bm != null) {
										a.setImageBitmap(bm);
									}
								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
								sendTextView("画像を表示しました。");
							}
						});
					} else if (data == 7) {
						Log.e("kyhwa", "7error");
					}
				} else {
					Log.e("kyhwa", "SC");
					sendTextView("SC");
				}
			} catch (final Exception e) {
				Log.e("kynwa", e.toString());
				sendTextView(e.toString());
			}
		}
	};

	public void sendTextView(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView1);
				error.setText(error.getText() + "\n" + a);
			}
		});
	}

	public void sendTextViewUnRet(final String a) {
		handler.post(new Runnable() {
			public void run() {
				TextView error = (TextView) findViewById(R.id.textView1);
				error.setText(error.getText() + a);
			}
		});
	}
}
