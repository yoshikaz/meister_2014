package com.example.controler;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends Activity {
	private int w,h; 
	private int w2,h2; 
	private int w3,h3; 
	Bitmap img;
	Bitmap img2;
	Bitmap img3;
	int count = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// タイトルを非表示にします。
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// splash.xmlをViewに指定します。
		setContentView(R.layout.splash);
		
		TextView ee = (TextView)findViewById(R.id.textView1);
		ee.setTypeface(Typeface.createFromAsset(getAssets(), 
        		"Coda-Regular.ttf"));
		TextView ee2 = (TextView)findViewById(R.id.textView2);
		ee2.setTypeface(Typeface.createFromAsset(getAssets(), 
        		"Coda-Regular.ttf"));
		
		Resources res = this.getBaseContext().getResources();
		img = BitmapFactory.decodeResource(res, R.drawable.splash1);//画像取り込み
		img2 = BitmapFactory.decodeResource(res, R.drawable.splash2);//画像取り込み
		img3 = BitmapFactory.decodeResource(res, R.drawable.splash);//画像取り込み
		w = img.getWidth();//画像の全体の幅
		h = img.getHeight();//画像の全体の高
		w2 = img2.getWidth();//画像の全体の幅
		h2 = img2.getHeight();//画像の全体の高
		w3 = img3.getWidth();//画像の全体の幅
		h3 = img3.getHeight();//画像の全体の高
		
		ImageView imging = (ImageView)findViewById(R.id.imageView1);
		ImageView imging2 = (ImageView)findViewById(R.id.imageView2);
		ImageView imging3 = (ImageView)findViewById(R.id.imageView3);
		/*Animation anim = AnimationUtils.loadAnimation(this, R.anim.rotate);
		imging.startAnimation(anim);*/
		RotateAnimation rotationImg = new RotateAnimation(0, 190, w/2, h/2);
		rotationImg.setDuration(3000);
		imging.startAnimation(rotationImg);
		
		RotateAnimation rotationImg2 = new RotateAnimation(0, -95, w2/2, h2/2);
		rotationImg2.setDuration(3000);
		imging2.startAnimation(rotationImg2);
		
		RotateAnimation rotationImg3 = new RotateAnimation(0, 45, w3/2, h3/2);
		rotationImg3.setDuration(3000);
		imging3.startAnimation(rotationImg3);

		Handler hdl = new Handler();
		hdl.postDelayed(new splashHandler(), 2900);
	}
	

	class splashHandler implements Runnable {
		public void run() {
			
			// スプラッシュ完了後に実行するActivityを指定します。
			Intent intent = new Intent(getApplication(), MainActivity.class);
			startActivity(intent);
			// SplashActivityを終了させます。
			SplashActivity.this.finish();
		}
	}
}