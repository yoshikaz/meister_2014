package com.example.controler;


/*
 * カメラの画像をよこせ。:001
 * カメラを上に動かせ。:010
 * カメラを下に動かせ。:011
 * 加速度センサーの値をよこせ。:100
 * 超音波センサーの値をよこせ。:101
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Random;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class MainService extends Service {

	private final static String IP = "1.2.3.4";
	private final static int PORT = 2000;
	private Socket socket;
	private InputStream in;
	private OutputStream out;
	private boolean mThreadActive = true;
	Integer InterbalInt = 0;

	SharedPreferences pref;
	SharedPreferences.Editor editor;

	@Override
	public void onCreate() {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.mThread = new Thread(null, mTask, "dataSendingService");
		this.mThread.start();

		return START_STICKY;
	}

	@Override
	public void onDestroy() {
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	private void sendData(byte[] data) {
		try {
			if (socket != null && socket.isConnected()) {
				out.write(data);
				out.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendPic() {
		byte[] data = null;
		int size;
		byte[] buffer = new byte[1024];

		try {
			if (socket != null && socket.isConnected()) {
				out.write(data);
				out.flush();
			}
			while (socket.isConnected() && socket != null) {
				size = in.read(buffer);
				if (size <= 0)
					continue;
				byte[] data2 = new byte[size];
				System.arraycopy(buffer, 0, data, 0, size);
				Log.d("kyhwa", "data length=" + data2.length);

				File cacheDir = getCacheDir();
				File file = new File(cacheDir, "liveMov" + ".jpg");
				OutputStream outn = new FileOutputStream(file);
				try {
					byte[] buf = new byte[10 * 1000 * 1000];
					int len = 0;
					while ((len = in.read(buffer)) > 0) {
						outn.write(buf, 0, len);
					}

					outn.flush();
				} finally {
					outn.close();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {

			while (mThreadActive) {
				try {
					Thread.sleep(InterbalInt);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				BufferedReader reader = null;
				BufferedWriter writer = null;
				
				Random rnd = new Random();
		        int ran = rnd.nextInt(100);

				try {
					if(ran == 24){
						if (socket.isConnected() && socket != null) {
							reader = new BufferedReader(new InputStreamReader(
									socket.getInputStream()));
							writer = new BufferedWriter(new OutputStreamWriter(
									socket.getOutputStream()));
						} else {
						}

						if (writer != null && reader != null) {
							writer.write("100");// 加速度センサー

							while (socket.isConnected() && socket != null) {
								String data = reader.readLine();
								if (data == null)
									continue;
								byte[] bytes = data.getBytes("UTF-8");
								
								pref = getSharedPreferences("camMorter",
										Activity.MODE_PRIVATE);
								editor = pref.edit();
								int eee=0;//加速度センサーの値をうまく変換して入れておくこと。
								
								editor.putInt("asenser", eee);
								editor.apply();
							}
						}
					}
					
					
					if (socket.isConnected() && socket != null) {

					} else {
						socket = new Socket(IP, PORT);
					}

					pref = getSharedPreferences("camMorter",
							Activity.MODE_PRIVATE);
					editor = pref.edit();

					int elem = pref.getInt("camMorter", 0);
					
					if (elem != 0) {
						if (socket.isConnected() && socket != null) {
							reader = new BufferedReader(new InputStreamReader(
									socket.getInputStream()));
							writer = new BufferedWriter(new OutputStreamWriter(
									socket.getOutputStream()));
						} else {
						}

						if (writer != null && reader != null) {
							
							
							if(elem>0){
								writer.write("010");// 写真要求じゃなくてカメラ動かせ―って要求
								elem--;
							}else{
								writer.write("011");// 写真要求じゃなくてカメラ動かせ―って要求
								elem++;
							}
							editor.putInt("camMorter", elem);
					    	editor.apply();
						}

					} else {

						if (socket.isConnected() && socket != null) {
							reader = new BufferedReader(new InputStreamReader(
									socket.getInputStream()));
							writer = new BufferedWriter(new OutputStreamWriter(
									socket.getOutputStream()));
						} else {
						}

						if (writer != null && reader != null) {
							writer.write("001");// 写真要求

							while (socket.isConnected() && socket != null) {
								String data = reader.readLine();
								if (data == null)
									continue;
								byte[] bytes = data.getBytes("UTF-8");

								File cacheDir = getCacheDir();
								File file = new File(cacheDir, "liveMov"
										+ ".jpg");
								OutputStream outn = new FileOutputStream(file);
								try {
									outn.write(bytes);
									outn.flush();
								} finally {
									outn.close();
								}
							}
						}
					}
				} catch (Exception e) {
					// 例外キャァァァチィィィィ。
				}
				try {
					reader.close();
					writer.close();
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.e("kyhwa","キャッチ。");
					  
				}

			}
		}
	};
	
	//H27/1/8の俺「うまく写真が取れないようなら色々順番、ifを変えること。」
	
	private Thread mThread;
}
