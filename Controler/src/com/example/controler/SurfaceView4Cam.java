package com.example.controler;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class SurfaceView4Cam extends SurfaceView implements SurfaceHolder.Callback, Runnable{
 
    public SurfaceView4Cam(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	private Bitmap mImage;
    private SurfaceHolder mHolder;
    private Thread mLooper;
    private int mHeight;            //画面の高さ
    private int mPositionTop = 0;   //表示位置(TOP:Y座標)
    private int mPositionLeft = 0;  //表示位置(LEFT:X座標)
    private long mTime =0;          //一つ前の描画時刻
    private long mLapTime =0;       //画面上部から下部に到達するまでの時間
     
    //SurfaceView変更時に呼び出される
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
     
        //スレッド処理を開始
        if(mLooper != null ){
            mHeight = height;
            mTime   = System.currentTimeMillis();
            mLooper.start();
        }
     
    }
    
	@Override
	public void run() {
		 
	    while (mLooper != null) {
	 
	        //描画処理
	        doDraw();
	 
	        //位置更新処理
	 
	        //処理落ちによるスローモーションをさけるため現在時刻を取得
	        long delta = System.currentTimeMillis() - mTime;
	        mTime      = System.currentTimeMillis();
	 
	        //次の描画位置
	        int nextPosition = (int)( ( delta / 1000.0 ) * 200 ); //1秒間に200px動くとして
	 
	        //描画範囲の設定
	        if(mPositionTop + nextPosition < mHeight  ){
	            mPositionTop += nextPosition;
	        }else{
	            //画面の縦移動が終わるまでの時間計測(一定であることが期待値)
	            //Log.d("VIEW","mLapTime:" + (mTime - mLapTime) );
	            mLapTime = mTime;
	 
	            //位置の初期化
	            mPositionTop = 0;
	        }
	    }
	}
	private void doDraw(){
	    //Canvasの取得(マルチスレッド環境対応のためLock)
	    Canvas canvas = mHolder.lockCanvas();
	 
	    Paint paint = new Paint();
	    //描画処理(Lock中なのでなるべく早く)
	    canvas.drawColor(Color.GRAY);
	    canvas.drawBitmap(mImage, mPositionLeft, mPositionTop, paint);
	 
	    //LockしたCanvasを解放、ほかの描画処理スレッドがあればそちらに。
	    mHolder.unlockCanvasAndPost(canvas);
	}
	
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mHolder = holder;
	    mLooper = new Thread(this);		
	}
	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		
	}

}
