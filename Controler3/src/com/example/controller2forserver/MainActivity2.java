package com.example.controller2forserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity2 extends Activity implements SensorEventListener {

	static int PORT = 8080;

	float[] defoltSenserVal = { (float) 0.0, (float) 0.0 };
	float[] nowSenserVal = { (float) 0.0, (float) 0.0 };
	float[] useSenserVal = { (float) 0.0, (float) 0.0 };

	protected final static double RAD2DEG = 180 / Math.PI;

	float[] rotationMatrix = new float[9];
	float[] gravity = new float[3];
	float[] geomagnetic = new float[3];
	float[] attitude = new float[3];

	int ControlBF;
	int ControlLR;
	int ControlCU;
	int DefControlBF;
	int DefControlLR;
	int DefControlCU;

	final Handler handler = new Handler();

	SensorManager sensorManager;
	private byte[] message = new byte[64];

	Bitmap bitmap = null;

	String IPAD;

	private Thread mTaskGetPic;
	private Thread mTaskConnect;
	private Thread mTaskSendCom;

	private Socket socket;
	BufferedReader reader = null;
	BufferedWriter writer = null;
	BufferedInputStream in = null;

	Boolean ControlFlag = false;

	int ControlSelect = 0;

	private View mTglBgWhite;
	private View mTglBgGreen;
	private ToggleButton mTglBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);

		Point size = new Point();
		WindowManager w2 = getWindowManager();
		w2.getDefaultDisplay().getSize(size);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		final ImageButton b = (ImageButton) findViewById(R.id.imageButton4);
		b.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				b.setVisibility(View.GONE);
				SharedPreferences prefs = getSharedPreferences("myprefs",
						Context.MODE_PRIVATE);
				IPAD = prefs.getString("IPAdress", "192.168.11.8");
				IPAD = "192.168.100.105";

				mTaskConnect = new Thread(null, ConnectSocket,
						"PicGettingService");
				mTaskConnect.start();
			}
		});
		
		Button sb3 = (Button) findViewById(R.id.button1);
		sb3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				ControlLR=112;ControlBF=1213;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
		
		Button sb4 = (Button) findViewById(R.id.button2);
		sb4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlBF=1222;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
		
		Button sb5 = (Button) findViewById(R.id.button3);
		sb5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlBF=1213;
				ControlLR=111;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
		
		Button sb6 = (Button) findViewById(R.id.button4);
		sb6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlLR=120;
				ControlBF=110;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
		
		Button sb7 = (Button) findViewById(R.id.button5);
		sb7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlBF=1212;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
		
		Button sb8 = (Button) findViewById(R.id.button6);
		sb8.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlLR=110;
				ControlBF=120;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});

		mTglBgWhite = (View) findViewById(R.id.tglBgWhite);
		mTglBgGreen = (View) findViewById(R.id.tglBgGreen);
		mTglBtn = (ToggleButton) findViewById(R.id.tglBtn);

		if (mTglBtn.isChecked()) {
			animOn(true);
		} else {
			animOff(true);
		}

		mTglBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					ControlFlag = isChecked;
					defoltSenserVal = nowSenserVal.clone();
				}
				if (isChecked) {
					animOn(false);
				} else {
					animOff(false);
				}
			}
		});

		ImageButton b2 = (ImageButton) findViewById(R.id.imageButton1);
		b2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlCU = 22;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});

		ImageButton b3 = (ImageButton) findViewById(R.id.imageButton3);
		b3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlCU = 21;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});

		ImageButton b11 = (ImageButton) findViewById(R.id.imageButton2);
		b11.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ControlCU = 20;
				mTaskSendCom = new Thread(null, SendCom,
						"PicGettingService");
				mTaskSendCom.start();
			}
		});
	}

	private void animOn(boolean durationZero) {
		Animation btnAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_btn_on);
		Animation bgWhiteAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_bg_white_on);
		Animation bgGreenAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_bg_green_on);

		if (durationZero) {
			btnAnim.setDuration(0);
			bgWhiteAnim.setDuration(0);
			bgGreenAnim.setDuration(0);
		}

		mTglBtn.startAnimation(btnAnim);
		mTglBgWhite.startAnimation(bgWhiteAnim);
		mTglBgGreen.startAnimation(bgGreenAnim);
	}

	private void animOff(boolean durationZero) {
		Animation btnAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_btn_off);
		Animation bgWhiteAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_bg_white_off);
		Animation bgGreenAnim = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.toggle_bg_green_off);

		if (durationZero) {
			btnAnim.setDuration(0);
			bgWhiteAnim.setDuration(0);
			bgGreenAnim.setDuration(0);
		}

		mTglBtn.startAnimation(btnAnim);
		mTglBgWhite.startAnimation(bgWhiteAnim);
		mTglBgGreen.startAnimation(bgGreenAnim);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static byte[] fromInt(int value) {
		int arraySize = Integer.SIZE / Byte.SIZE;
		ByteBuffer buffer = ByteBuffer.allocate(arraySize);
		return buffer.putInt(value).array();
	}

	private Runnable ConnectSocket = new Runnable() {
		@Override
		public void run() {
			try {
				socket = new Socket(IPAD, PORT);
				Log.e("kyhwa", "shu");
				if (socket.isConnected() && socket != null) {
					Log.e("kyhwa", "connected");
					reader = new BufferedReader(new InputStreamReader(
							socket.getInputStream()));
					writer = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream()));
					Log.e("kyhwa", "opend");

					mTaskGetPic = new Thread(null, GetPic, "PicGettingService");
					mTaskGetPic.start();

				}

			} catch (UnknownHostException e) {
				e.printStackTrace();
				Log.e("kyhwa", e.toString());
			} catch (IOException e) {
				e.printStackTrace();
				Log.e("kyhwa", e.toString());
			}
		}
	};

	private Runnable GetPic = new Runnable() {
		@Override
		public void run() {

			while (true) {
				if (!socket.isConnected() || socket == null) {
					try {
						socket = new Socket(IPAD, PORT);
						reader = new BufferedReader(new InputStreamReader(
								socket.getInputStream()));
						writer = new BufferedWriter(new OutputStreamWriter(
								socket.getOutputStream()));
					} catch (Exception e) {

					}
				}
				if (socket.isConnected() && socket != null) {
					try {
						bitmap = null;
						in = new BufferedInputStream(socket.getInputStream());
						try {
							BitmapFactory.Options imageOptions = new BitmapFactory.Options();
							imageOptions.inPreferredConfig = Bitmap.Config.RGB_565;
							bitmap = BitmapFactory.decodeStream(in, null,
									imageOptions);
							if (bitmap != null) {
								handler.post(new Runnable() {
									public void run() {
										ImageView d = (ImageView) findViewById(R.id.imageView1);
										d.setImageDrawable(null);
										d.setImageBitmap(bitmap);
										Log.e("kyhwa", "painted");
									}
								});
							}

						} catch (Exception e) {
						}
					} catch (IOException e) {
						Log.e("kyhwa", e.toString());
					}
				} else {
				}
			}
		}
	};

	private Runnable SendCom = new Runnable() {
		@Override
		public void run() {
			if (!socket.isConnected() || socket == null) {
				try {
					socket = new Socket(IPAD, PORT);
					reader = new BufferedReader(new InputStreamReader(
							socket.getInputStream()));
					writer = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream()));
				} catch (Exception e) {

				}
			}
			if (socket.isConnected() && socket != null) {
				try {
					if (DefControlLR != ControlLR && ControlLR != 0) {
						writer.write(ControlLR + "\n");
						writer.flush();
						DefControlLR = ControlLR;
					}
					if (DefControlBF != ControlBF && ControlBF != 0) {
						writer.write(ControlBF + "\n");
						writer.flush();
						DefControlBF = ControlBF;
					}
					if (DefControlCU != ControlCU && ControlCU != 0) {
						writer.write(ControlCU + "\n");
						writer.flush();
						DefControlCU = ControlCU;
					}
				} catch (IOException e) {
					Log.e("kyhwa", e.toString());
				}
			}
		}
	};

	public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height,
				null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
		byte[] jdata = baos.toByteArray();
		BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
		bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length,
				bitmapFatoryOptions);
		return bmp;
	}

	@Override
	protected void onResume() {
		super.onResume();

		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_UI);
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_UI);
	}

	public void onPause() {
		super.onPause();
		sensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_MAGNETIC_FIELD:
			geomagnetic = event.values.clone();
			break;
		case Sensor.TYPE_ACCELEROMETER:
			gravity = event.values.clone();
			break;
		}

		if (geomagnetic != null && gravity != null && ControlFlag) {

			SensorManager.getRotationMatrix(rotationMatrix, null, gravity,
					geomagnetic);
			SensorManager.getOrientation(rotationMatrix, attitude);

			nowSenserVal[0] = (int) (attitude[2] * RAD2DEG);
			nowSenserVal[1] = (int) (attitude[1] * RAD2DEG);
			useSenserVal[0] = (int) (nowSenserVal[0] - defoltSenserVal[0]);
			useSenserVal[1] = (int) (nowSenserVal[1] - defoltSenserVal[1]);

			/*ControlBF = 0;
			ControlLR = 0;

			if (useSenserVal[0] > 0 && useSenserVal[1] > 0) {
				if (useSenserVal[0] > 90) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1215;
						ControlLR = 112;
					} else {
						ControlBF = 1215;
					}
				} else if (useSenserVal[0] > 75) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1214;
						ControlLR = 112;
					} else {
						ControlBF = 1214;
					}
				} else if (useSenserVal[0] > 50) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1213;
						ControlLR = 112;
					} else {
						ControlBF = 1213;
					}
				} else if (useSenserVal[0] > 35) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1212;
						ControlLR = 112;
					} else {
						ControlBF = 1212;
					}
				} else if (useSenserVal[0] > 15) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1211;
						ControlLR = 112;
					} else {
						ControlBF = 1211;
					}
				}
			} else if (useSenserVal[0] > 0 && useSenserVal[1] < 0) {
				if (useSenserVal[0] > 90) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1215;
						ControlLR = 111;
					} else {
						ControlBF = 1215;
					}
				} else if (useSenserVal[0] > 75) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1214;
						ControlLR = 111;
					} else {
						ControlBF = 1214;
					}
				} else if (useSenserVal[0] > 50) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1213;
						ControlLR = 111;
					} else {
						ControlBF = 1213;
					}
				} else if (useSenserVal[0] > 35) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1212;
						ControlLR = 111;
					} else {
						ControlBF = 1212;
					}
				} else if (useSenserVal[0] > 15) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1211;
						ControlLR = 111;
					} else {
						ControlBF = 1211;
					}
				}
			} else if (useSenserVal[0] < 0 && useSenserVal[1] > 0) {
				if (useSenserVal[0] < -90) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1225;
						ControlLR = 112;
					} else {
						ControlBF = 1225;
					}
				} else if (useSenserVal[0] < -75) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1224;
						ControlLR = 112;
					} else {
						ControlBF = 1224;
					}
				} else if (useSenserVal[0] < -50) {
					if (useSenserVal[1] > 30) {
						ControlBF = 1223;
						ControlLR = 112;
					} else {
						ControlBF = 1223;
					}
				} else if (useSenserVal[0] < -35) {
					if (useSenserVal[1] > 30) {
						ControlBF = 1222;
						ControlLR = 112;
					} else {
						ControlBF = 1222;
					}
				} else if (useSenserVal[0] < -15) {
					if (useSenserVal[1] > 15) {
						ControlBF = 1221;
						ControlLR = 112;
					} else {
						ControlBF = 1221;
					}
				}
			} else if (useSenserVal[0] < 0 && useSenserVal[1] < 0) {
				if (useSenserVal[0] < -90) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1225;
						ControlLR = 111;
					} else {
						ControlBF = 1225;
					}
				} else if (useSenserVal[0] < -75) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1224;
						ControlLR = 111;
					} else {
						ControlBF = 1224;
					}
				} else if (useSenserVal[0] < -50) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1223;
						ControlLR = 111;
					} else {
						ControlBF = 1223;
					}
				} else if (useSenserVal[0] < -35) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1222;
						ControlLR = 111;
					} else {
						ControlBF = 1222;
					}
				} else if (useSenserVal[0] < -15) {
					if (useSenserVal[1] < -15) {
						ControlBF = 1221;
						ControlLR = 111;
					} else {
						ControlBF = 1221;
					}
				}
			}
			if (ControlBF == 0) {
				ControlBF = 120;
			}
			if (ControlLR == 0) {
				ControlLR = 110;
			}*/
			
			mTaskSendCom = new Thread(null, SendCom,
					"PicGettingService");
			mTaskSendCom.start();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}
}
