package com.example.controller2forserver;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.widget.RadioButton;
 
public class CustomRadioButton extends RadioButton {
 
    public static final String POSITION = "position";
    public static final String POSITION_LEFT = "left";
    public static final String POSITION_CENTER = "center";
    public static final String POSITION_RIGHT = "right";
 
    /** ボタンカラー */
    private int mColor;
 
    /** 角丸半径 */
    private float mRadius;
 
    /** ポジション(左側、中央、右側) */
    private String mPosition = POSITION_LEFT;
 
    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
 
        // layout.xml で指定したポジション(左側、中央、右側)を取得して保持します。
        mPosition = attrs.getAttributeValue(null, POSITION);
 
        // あらかじめ値を取得しておきます。
        mRadius = getResources().getDimension(R.dimen.radio_corner_radius);
        mColor = getResources().getColor(R.color.radio_color);
    }
 
    @Override
    protected void onDraw(Canvas canvas) {
        if (isChecked()) {
            // 背景画像を生成します。
            Bitmap bgBm = createBg();
 
            // マスク画像を生成します。
            Bitmap maskBm = createMask();
 
            // マスク処理を行います。
            mask(bgBm, maskBm, canvas);
        } else {
            // 未選択時はdrawableで表示します。
            super.onDraw(canvas);
        }
    }
 
    private Bitmap createBg() {
        Bitmap bgBm = Bitmap.createBitmap(getWidth(), getHeight(),
                Bitmap.Config.ARGB_8888);
 
        float[] outerR = null;
        if (POSITION_LEFT.equals(mPosition)) {
            outerR = new float[] { mRadius, mRadius, 0, 0, 0, 0, mRadius,
                    mRadius };
 
        } else if (POSITION_CENTER.equals(mPosition)) {
            outerR = new float[] { 0, 0, 0, 0, 0, 0, 0, 0 };
 
        } else if (POSITION_RIGHT.equals(mPosition)) {
            outerR = new float[] { 0, 0, mRadius, mRadius, mRadius, mRadius, 0,
                    0 };
        }
 
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(
                outerR, null, null));
        shapeDrawable.setBounds(0, 0, getWidth(), getHeight());
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setColor(mColor);
 
        Canvas bgCv = new Canvas(bgBm);
        shapeDrawable.draw(bgCv);
        return bgBm;
    }
 
    private Bitmap createMask() {
        Bitmap maskBm = Bitmap.createBitmap(getWidth(), getHeight(),
                Bitmap.Config.ARGB_8888);
        Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setTextSize(getTextSize());
        textPaint.setTextAlign(Paint.Align.CENTER);
 
        new Canvas(maskBm).drawText(getText().toString(), getWidth() / 2.0f,
                getBaseline(), textPaint);
        return maskBm;
    }
 
    private void mask(Bitmap baseBm, Bitmap maskBm, Canvas canvas) {
        Bitmap maskedBm = Bitmap.createBitmap(getWidth(), getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas maskedCv = new Canvas(maskedBm);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setFilterBitmap(false);
        maskedCv.drawBitmap(baseBm, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        maskedCv.drawBitmap(maskBm, 0, 0, paint);
        paint.setXfermode(null);
 
        canvas.drawBitmap(maskedBm, 0, 0, new Paint(Paint.ANTI_ALIAS_FLAG));
    }
}