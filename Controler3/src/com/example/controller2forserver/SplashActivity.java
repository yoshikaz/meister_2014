package com.example.controller2forserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// タイトルを非表示にします。
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// splash.xmlをViewに指定します。
		setContentView(R.layout.splash);

		final TextView t1 = (TextView) findViewById(R.id.textView1);
		t1.setText("Server Mode");
		t1.setTypeface(Typeface.createFromAsset(getAssets(),
				"mikiyucrayon2.TTF"));

		final TextView t2 = (TextView) findViewById(R.id.textView2);
		t2.setVisibility(View.GONE);
		t2.setTypeface(Typeface.createFromAsset(getAssets(),
				"mikiyucrayon2.TTF"));
		
		final EditText e1 = (EditText) findViewById(R.id.editText1);
		e1.setVisibility(View.GONE);
		SharedPreferences prefs = getSharedPreferences("myprefs", Context.MODE_PRIVATE);
		String IPAD = prefs.getString("IPAdress", "192.168.11.8");
		e1.setText(IPAD);
		
		final ImageButton bf = (ImageButton) findViewById(R.id.buttonF);

		Button bl = (Button) findViewById(R.id.testleftBtn);
		bl.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				t1.setText("Server Mode");
				bf.setVisibility(View.VISIBLE);
				t2.setVisibility(View.GONE);
				e1.setVisibility(View.GONE);
			}
		});

		Button bc = (Button) findViewById(R.id.testcenterBtn);
		bc.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				t1.setText("Cliant Mode");
				bf.setVisibility(View.VISIBLE);
				t2.setVisibility(View.GONE);
				e1.setVisibility(View.GONE);
			}
		});
		Button br = (Button) findViewById(R.id.testrightBtn);
		br.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				t1.setText("Setting Mode");
				bf.setVisibility(View.GONE);
				t2.setVisibility(View.VISIBLE);
				e1.setVisibility(View.VISIBLE);
			}
		});

		bf.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (t1.getText().toString() == "Server Mode") {
					Intent cameraAct = new Intent(getApplicationContext(),
							ServerActivity.class);
					startActivity(cameraAct);
				} else if (t1.getText().toString() == "Cliant Mode") {
					if (e1.getText().toString() != "") {
						SharedPreferences prefs = getSharedPreferences(
								"myprefs", Context.MODE_PRIVATE);
						SharedPreferences.Editor editor = prefs.edit();
						editor.putString("IPAdress", e1.getText().toString());
						editor.apply();
					}
					Intent cameraAct = new Intent(getApplicationContext(),
							MainActivity2.class);
					startActivity(cameraAct);
				} else {

				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public void onPause() {
		super.onPause();
	}
}