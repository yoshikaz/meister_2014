package com.example.controller2forserver;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;

import android.hardware.Camera;
import android.util.Log;
import android.widget.Toast;

public class ThreadDataSendServer extends Thread {
	// 変数の宣言
	private byte[] data;
	Socket mSocket;
	BufferedReader reader;
	BufferedWriter writer;
	BufferedOutputStream in;
	Boolean flag1;

	// スレッド作成時に実行される処理
	public ThreadDataSendServer(byte[] data, Socket mSocket,
			BufferedReader reader, BufferedWriter writer,
			BufferedOutputStream in, Boolean flag1) {
		this.data = data;
		this.mSocket = mSocket;
		this.reader = reader;
		this.writer = writer;
		this.in = in;
		this.flag1 = flag1;
		Log.e("kyhwa", "init");
	}

	// スレッド実行時の処理
	public void run() {
		try {
			Log.e("kyhwa", "send start");
			
			in = new BufferedOutputStream(mSocket.getOutputStream());
			in.write(data, 0, data.length);
			in.flush();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	// スレッド終了時に呼び出し
	public void stopThread() {
	}
}