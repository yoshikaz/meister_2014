package com.example.controller2forserver;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

public class ServerActivity extends Activity {

	Activity mActivity;
	Camera mCamera; // カメラインスタンス
	private Thread mTaskSendPic2;
	private Thread mTaskSendPic;
	private Thread mTaskRecvCom;
	private Thread opn;
	int Count = 0;

	private UsbManager mUsbManager;
	private UsbDevice mDevice;
	private UsbDeviceConnection mConnection;
	private UsbEndpoint mEndpointOut;

	final Handler handler = new Handler();

	ServerSocket mServer;
	private Socket mSocket;

	private Thread mThread;

	int Control;

	BufferedReader reader = null;
	BufferedWriter writer = null;
	BufferedOutputStream in = null;

	Boolean flag1 = false;
	Boolean flag2 = true;
	Boolean flag3 = false;

	private byte[] message = new byte[64];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;

		ActionBar actionBar = getActionBar();
		actionBar.hide();

		FrameLayout frameLayout = new FrameLayout(this);

		SurfaceView sv = new SurfaceView(this);
		SurfaceHolder sh = sv.getHolder();
		sh.addCallback(new SurfaceHolderCbk());
		sh.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.WRAP_CONTENT,
				FrameLayout.LayoutParams.WRAP_CONTENT);

		frameLayout.addView(sv);
		setContentView(frameLayout);

		mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

		opn = new Thread(null, Opn, "PicGettingService");
		opn.start();
	}

	private Runnable Opn = new Runnable() {
		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				mServer = new ServerSocket(8080);
				mSocket = mServer.accept();
				Log.e("kyhwa", "Socket");
				reader = new BufferedReader(new InputStreamReader(
						mSocket.getInputStream()));
				writer = new BufferedWriter(new OutputStreamWriter(
						mSocket.getOutputStream()));
				Log.e("kyhwa", "OPen");
				mTaskRecvCom = new Thread(null, recvCom, "PicGettingService");
				mTaskRecvCom.start();
				mTaskSendPic = new Thread(null, sendPic, "PicGettingService");
				mTaskSendPic.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	};

	private Runnable sendPic = new Runnable() {
		@Override
		public void run() {
			flag1 = false;
			while (true) {
				if (!mSocket.isConnected() || mSocket == null) {
					try {
						mServer = new ServerSocket(8080);
						mSocket = mServer.accept();

						reader = new BufferedReader(new InputStreamReader(
								mSocket.getInputStream()));
						writer = new BufferedWriter(new OutputStreamWriter(
								mSocket.getOutputStream()));
					} catch (Exception e) {
					}
				}
				if (!flag1) {
					flag1 = true;
					mCamera.setPreviewCallback(previewCallback);
				}
			}
		}
	};

	private Runnable recvCom = new Runnable() {
		@Override
		public void run() {
			while (true) {
				if (!mSocket.isConnected() || mSocket == null) {
					try {
						mServer = new ServerSocket(8080);
						mSocket = mServer.accept();

						reader = new BufferedReader(new InputStreamReader(
								mSocket.getInputStream()));
						writer = new BufferedWriter(new OutputStreamWriter(
								mSocket.getOutputStream()));
					} catch (Exception e) {
					}
				}
				try {
					Log.e("kyhwa", "read");
					String elm = reader.readLine();
					Control = Integer.parseInt(elm);
					if (Control != 0 && Control != -1) {
						Log.e("kyhwa", "Control" + Control);

						mThread = new Thread(null, mTask, "dataSendingService");
						mThread.start();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	};

	private Runnable mTask = new Runnable() {
		@Override
		public void run() {
			sendUSB(Control);
		}
	};

	private void sendUSB(int arg) {
		try {
			switch (arg) {
			case 1211:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x1;
				message[3] = 0x1;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1212:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x1;
				message[3] = 0x2;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1213:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x1;
				message[3] = 0x3;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1214:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x1;
				message[3] = 0x4;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1215:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x1;
				message[3] = 0x5;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1221:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x2;
				message[3] = 0x1;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1222:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x2;
				message[3] = 0x2;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1223:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x2;
				message[3] = 0x3;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1224:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x2;
				message[3] = 0x4;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 1225:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x2;
				message[3] = 0x5;
				mConnection.bulkTransfer(mEndpointOut, message, 4, 0);
				break;
			case 120:
				message[0] = 0x1;
				message[1] = 0x2;
				message[2] = 0x0;
				mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
				break;
			case 110:
				message[0] = 0x1;
				message[1] = 0x1;
				message[2] = 0x0;
				mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
				break;
			case 111:
				message[0] = 0x1;
				message[1] = 0x1;
				message[2] = 0x1;
				mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
				break;
			case 112:
				message[0] = 0x1;
				message[1] = 0x1;
				message[2] = 0x2;
				mConnection.bulkTransfer(mEndpointOut, message, 3, 0);
				break;
			case 21:
				message[0] = 0x2;
				message[1] = 0x1;
				mConnection.bulkTransfer(mEndpointOut, message, 2, 0);
				break;
			case 22:
				message[0] = 0x2;
				message[1] = 0x2;
				mConnection.bulkTransfer(mEndpointOut, message, 2, 0);
				break;
			case 20:
				message[0] = 0x2;
				message[1] = 0x0;
				mConnection.bulkTransfer(mEndpointOut, message, 2, 0);
				break;
			}
		} catch (Exception e) {

		}
	}

	private void setDevice(UsbDevice device) {
		if (device.getInterfaceCount() != 1) {
			Log.e("kyhwa", "インタフェース発見できませんでした");
			Toast.makeText(this, "インタフェース発見できませんでした", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		UsbInterface intf = device.getInterface(0);
		if (intf.getEndpointCount() < 2) {
			Log.e("kyhwa", "エンドポイントを検出できませんでした");
			return;
		}
		Log.e("kyhwa", "エンドポイント数 =" + intf.getEndpointCount());

		UsbEndpoint epout = intf.getEndpoint(0);
		if (epout.getType() != UsbConstants.USB_ENDPOINT_XFER_BULK) {
			Log.e("kyhwa", "OUTエンドポイントがバルクタイプではありません");
			return;
		}

		mDevice = device;
		mEndpointOut = epout;

		if (mDevice != null) {
			UsbDeviceConnection connection = mUsbManager.openDevice(mDevice);
			if (connection != null && connection.claimInterface(intf, true)) {
				mConnection = connection;
				Log.e("kyhwa", "USBデバイス接続正常完了");
				Toast.makeText(getApplicationContext(), "USBデバイス接続正常完了",
						Toast.LENGTH_LONG).show();
			} else {
				mConnection = null;
				Log.e("kyhwa", "USBデバイス接続失敗");
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Intent intent = getIntent();
		String action = intent.getAction();
		UsbDevice device = (UsbDevice) intent
				.getParcelableExtra(UsbManager.EXTRA_DEVICE);
		if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
			// text1.setText("アプリケーション再起動");
			setDevice(device);
		} else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(device)) {
			setDevice(null);
			mConnection.close();
			// text1.setText("デバイス　デタッチで終了");
		}
	}

	public class SurfaceHolderCbk implements SurfaceHolder.Callback {

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			mCamera.startPreview();
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			mCamera = Camera.open();
			try {
				mCamera.setPreviewDisplay(holder);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	public Bitmap getBitmapImageFromYUV(byte[] data, int width, int height) {
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height,
				null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, width, height), 100, baos);
		byte[] jdata = baos.toByteArray();
		BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
		bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length,
				bitmapFatoryOptions);
		return bmp;
	}

	private PreviewCallback previewCallback = new PreviewCallback() {
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			mCamera.setPreviewCallback(null);
			Log.e("kyhwa", "Cam");
			Bitmap bmp = getBitmapImageFromYUV(data, mCamera.getParameters()
					.getPreviewSize().width, mCamera.getParameters()
					.getPreviewSize().height);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(CompressFormat.PNG, 1, baos);
			byte[] bytes = baos.toByteArray();

			ThreadDataSendServer thread1 = new ThreadDataSendServer(bytes,
					mSocket, reader, writer, in, flag1);
			Log.e("kyhwa", "thread");
			thread1.start();
			try {
				thread1.join();
			} catch (Exception e) {
			}
			flag1 = false;
		}
	};
}