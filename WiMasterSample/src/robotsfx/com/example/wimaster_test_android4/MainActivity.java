﻿package robotsfx.com.example.wimaster_test_android4;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	private final static String TAG="WiMaster_test4";
	private final static String IP="1.2.3.4";
	private final static int PORT=2000;
	private final static int WifiComWhat = 1;
	private Socket socket;
	private InputStream  in;
	private OutputStream out;
	private static TextView sensorText;
	private Button onButton;
	private Button onnButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//---UI---
		sensorText = (TextView)findViewById(R.id.sensor_text);
		onButton = (Button)findViewById(R.id.on_button);
		onButton.setOnClickListener(new onButtonClick());
		onnButton = (Button)findViewById(R.id.off_button);
		onnButton.setOnClickListener(new onButtonClick());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	////////////////////////////////////////////////
    //	onResume
	////////////////////////////////////////////////
    @Override
    public void onResume(){
    	super.onResume();	
    	Thread thread = new Thread(){
    		public void run(){
    			try{
    				connect(IP, PORT);
    			}catch(Exception e){
    				e.printStackTrace();
    			}
    		}
    	};
    	thread.start();
    } 
    
	////////////////////////////////////////////////
    //	onPause
	////////////////////////////////////////////////
    @Override
    public void onPause(){
    	//pause譎ゅ↓繧ｽ繧ｱ繝�ヨ繧帝哩縺倥ｋ
    	try{
    		socket.close();
    		socket=null;
    	}catch(Exception e){	
    	}
    	super.onPause();
    }
    
	/////////////////////////////////////////////////
    //	謗･邯�
	/////////////////////////////////////////////////
    private void connect(String ip,int port) {
        Log.v(TAG,"connect start");
        int size;
        byte[] buffer=new byte[1024];
        try {
            //繧ｽ繧ｱ繝�ヨ謗･邯�
            socket=new Socket(ip,port);            
            if( socket.isConnected() && socket != null)
            {
                in =socket.getInputStream();
                out=socket.getOutputStream();
                Log.v("connect","謗･邯壽�蜉�");
            }
            else
            {
                Log.v("connect","謗･邯壼､ｱ謨�");
            }
            //蜿嶺ｿ｡繝ｫ繝ｼ繝�
            while( socket.isConnected() && socket != null){
            	size = in.read(buffer);
            	if(size<=0) continue;
    			//繝��繧ｿ繧偵ワ繝ｳ繝峨Λ縺ｫ貂｡縺�
    			byte[] data = new byte[size];
    			System.arraycopy(buffer, 0, data, 0, size);
    			Log.d(TAG,"data length="+data.length);
				handler.obtainMessage(WifiComWhat, data).sendToTarget();
            }
            
        } catch (Exception e) {
            Log.v("connect", e.toString() );
        }
    }
    
	/////////////////////////////////////////////////
	//	繝懊ち繝ｳ蜃ｦ逅�
	/////////////////////////////////////////////////
	class onButtonClick implements OnClickListener{
		@Override
		public void onClick(View v){
			switch (v.getId()) {
			case R.id.on_button:		//on
				sendLedOnCommand();
				break;
			case R.id.off_button:		//off
				sendLedOffCommand();
				break;
			
			}
		}
	}
	
	/////////////////////////////////////////////////
	//	LED謫堺ｽ懷多莉､騾∽ｿ｡
	/////////////////////////////////////////////////
	private void sendLedOnCommand(){
		try{
			byte[] data = new byte[1];
			data[0] = (byte)1;		//on command
			sendData(data);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private void sendLedOffCommand(){
		try{
			byte[] data = new byte[1];
			data[0] = (byte)0;		//off command
			sendData(data);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	////////////////////////////////////////////////
    //	繝��繧ｿ騾∽ｿ｡
	////////////////////////////////////////////////	
    private void sendData(byte[] data){
    	try{
			//繝��繧ｿ縺ｮ騾∽ｿ｡
			if(socket!=null && socket.isConnected()){
				out.write(data);
				out.flush();
			}
		}catch(Exception e){
			Log.d(TAG,"騾∽ｿ｡��ｼ∝､ｱ謨励＠縺ｾ縺励◆");
			e.printStackTrace();
		}
    }
    
    ////////////////////////////////////////////////
    //	繧､繝吶Φ繝医ワ繝ｳ繝峨Λ(蜿嶺ｿ｡)
	////////////////////////////////////////////////	
    private static Handler handler = new Handler() {
    	@Override
    	public void handleMessage(Message msg) {
			switch(msg.what)
			{	
			case WifiComWhat:
				byte[] data = (byte[])msg.obj;
				//蜿嶺ｿ｡繝��繧ｿdebug陦ｨ遉ｺ
				String str = "";
				for(int i=0; i<data.length; i++){
					str += String.valueOf(data[i]) + ",";
				}
				Log.d(TAG,"data="+str);
				chkRcvData(data);
				break;
			default:
				break;
			}	//switch
    	} //handleMessage
    }; //handler
    
    ////////////////////////////////////////////////
    //	蜿嶺ｿ｡繝��繧ｿ縺ｮ陦ｨ遉ｺ
	////////////////////////////////////////////////
    private static void chkRcvData(byte[] data){
    		String str = String.valueOf(convByteToInt(data[0]));	//*1
    		sensorText.setText(str);
    }

    private static int convByteToInt(byte b){
		int i = (int)b;
		i &= 0x000000FF;
		return i;
	}
    
}
